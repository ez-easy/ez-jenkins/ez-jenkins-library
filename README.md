![ez logo](https://gitlab.com/ez-mentor/ez-mentor-resources/-/raw/main/images/EZ-MENTOR-Transparent-small.png)
# <font color=blue size="16">EZJEL</font> (ez-jenkins-library) - easy-to-use Jenkins-shared-library

## EZJEL (ez-jenkins-library) - what is it for?

## EZJEL (ez-jenkins-library) - how to set Jenkins to use it?
Plugins that must be installed:
- ansiColor
- pipeline utility steps
- build-user-vars-plugin
- slack notification

## EZJEL (ez-jenkins-library) - how to use it inside your GIT repository?

## EZJEL (ez-jenkins-library) - ez.yaml usage

